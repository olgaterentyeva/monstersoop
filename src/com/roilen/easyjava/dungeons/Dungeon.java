package com.roilen.easyjava.dungeons;
import com.roilen.easyjava.Party;
import com.roilen.easyjava.monsteres.MonsterClass;

public class Dungeon implements BasicDungeon {
    private boolean isOpened = false;
    private MonsterClass [] monsters;
    @Override
    public boolean open(Party party) {
            if (party.getPartyMembers().length < 3) {
                System.out.println("Not enough party members");
                isOpened = false;
            } else {
                System.out.println("Welcome! \nGet ready for the adventure!");
                isOpened = true;
            }
            return isOpened;
        }
    }

