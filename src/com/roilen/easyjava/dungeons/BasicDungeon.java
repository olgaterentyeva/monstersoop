package com.roilen.easyjava.dungeons;

import com.roilen.easyjava.Party;

public interface BasicDungeon {
    boolean open (Party party);
}

