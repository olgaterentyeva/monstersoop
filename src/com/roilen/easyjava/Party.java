package com.roilen.easyjava;

import com.roilen.easyjava.classes.CharacterClass;
import com.roilen.easyjava.dungeons.Dungeon;

public class Party {
    private CharacterClass[] partyMembers;
    private Dungeon dungeon;
    public Party(CharacterClass... members) { // списки
        partyMembers = new CharacterClass[members.length];
        for (int i = 0; i < partyMembers.length; i++) {
            partyMembers[i] = members[i];
        }
    }
    public boolean enterDungeon(Dungeon dungeon) {
        return dungeon.open(this);
    }
    public void runDungeon() {
      // тут можно сделать массив, который проходится по атаке монстров, если нужно усложнить
    }
    public CharacterClass[] getPartyMembers() {
        return partyMembers;
    }
    public void setPartyMembers(CharacterClass[] partyMembers) {
        this.partyMembers = partyMembers;
    }
    public void info() {
        for (CharacterClass partyMember : partyMembers) {
            partyMember.info();
        }
    }
    public Dungeon getDungeon() {
        return dungeon;
    }
    public void setDungeon(Dungeon dungeon) {
        this.dungeon = dungeon;
    }
}
