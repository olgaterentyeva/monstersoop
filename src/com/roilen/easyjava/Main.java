package com.roilen.easyjava;

import com.roilen.easyjava.classes.character.Archer;
import com.roilen.easyjava.classes.character.Healer;
import com.roilen.easyjava.classes.character.Mage;
import com.roilen.easyjava.classes.character.Warrior;
import com.roilen.easyjava.dungeons.Dungeon;

public class Main {
    public static void main(String[] args) {
        Party party = new Party(new Mage("Gandalf"), new Warrior("Aragorn"),
                new Archer("Legolas"), new Healer("Radogast"));
        party.info();
        Dungeon dungeon = new Dungeon();
        if (party.enterDungeon(dungeon)) {
                party.setDungeon(dungeon);
                party.runDungeon();
        }
        else System.out.println("GAME OVER!");
    }
}
