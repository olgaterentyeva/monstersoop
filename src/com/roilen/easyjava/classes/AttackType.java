package com.roilen.easyjava.classes;

public enum AttackType { // конструктор перечисления
    AIR, EARTH, FIRE, MAGICAL, MAGICAL_RANGED, PHYSICAL, PHYSICAL_RANGED, WATER;
}
