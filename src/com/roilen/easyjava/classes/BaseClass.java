package com.roilen.easyjava.classes;

public interface BaseClass {
    // у всех героев есть общие черты, котры находятся в отдельном классе, который базируется на интерфейсе
    void attack();
    void restoreHealth(int amount);
    void loseHealth(int amount);
    void restoreMana(int amount);
    void loseMana(int amount);
    void levelUp();
    void info();
}
